//
//  TimeCalcKeyboardView.swift
//  TimeCalc
//
//  Created by Kenta Nakai on 2015/04/27.
//  Copyright (c) 2015年 UROURO. All rights reserved.
//

import UIKit

class TimeCalcKeyboardView : UIView {

    class func view() -> TimeCalcKeyboardView {
        return NSBundle.mainBundle().loadNibNamed("TimeCalcKeyboardView", owner: 0, options: nil)[0] as! TimeCalcKeyboardView
    }

    @IBAction func onNumberKey(sender: UIButton) {
    }

    @IBAction func onAddKey(sender: UIButton) {
    }

    @IBAction func onSubtractKey(sender: UIButton) {
    }

    @IBAction func onMakeKey(sender: UIButton) {
    }

    @IBAction func onColonKey(sender: UIButton) {
    }

    @IBAction func onBackspaceKey(sender: UIButton) {
    }
}
